<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Get prime</title>
</head>
<body>


<script>


    function isPrime(num) {
        for ( var i = 2; i < num; i++ ) {
            if ( num % i === 0 ) {
                return false;
            }
        }
        return true;
    }


    for ( var i = 1; i < 100; i++ ) {
        if(i!=1) {
            if (isPrime(i)) {
                document.write(i + " ")
            }
        }
    }



</script>


</body>
</html>